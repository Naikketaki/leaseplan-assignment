package steps;
import io.cucumber.java.en.Given;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class Spotify_Implimentations {
	
	@Step
	public void GetStatuscode(){
		given().
		when().
		get("https://developer.spotify.com/").
		then().
		//getStatusCode().
        //System.out.println("My response is" + code);
        assertThat().statusCode(200);
	}
	
	@Step
	public void GetString(){
		//Response resp=get("https://developer.spotify.com/");
        String responsestring=get("https://developer.spotify.com/").asString();
        System.out.println("My response is" + responsestring);
        /*Assert.assertEquals(responsestring, 200)*/;
        int time= (int) get("https://developer.spotify.com/").getTime();
        System.out.println("Your time is" + time);
	}
	
	@Step
	public void CheckHeader(){
	given().
	when().
	get("https://any-api.com/spotify_com/spotify_com/docs/_artists_id_related_artists/GET").
	then().
	assertThat().statusCode(200).
	and().
	assertThat().header("Content-Type",equalTo("text/html; charset=UTF-8"));
	
	}

}
