package ketakitestcases;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static io.restassured.RestAssured.*;
//import io.restassured.response.Response;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import steps.Spotify_Implimentations;

import static org.hamcrest.Matcher.*;
import static org.hamcrest.Matchers.equalTo;

import org.hamcrest.Matcher;

//import org.hamcrest.Matcher;

@RunWith(SerenityRunner.class)
public class MyTestCase {
	
	@Managed
	WebDriver driver;
	
	@Steps
	Spotify_Implimentations impl;
	
	@Test
	public void TestStatuscode(){
       
        impl.GetStatuscode();
	
	}
	
	@Test
	public void TestString(){
		
		impl.GetString();
	
	}
	
	@Test
	public void TestHeaders(){
	
		impl.CheckHeader();
		
		
	}



	

}
